import { css } from '@emotion/react';

import theme from '@/assets/styles/theme';

const value = theme.typography.weight;

const generatedStyle = () => {
  let result = '';
  const values = new Map(Object.entries(value));
  values.forEach((val, valKey) => {
    result += `.${valKey}{ font-weight: ${val} }`;
  });
  return result;
};

const weight = css`
  ${generatedStyle()}
`;

export default weight;
