import { css } from '@emotion/react';
import theme from '@/assets/styles/theme';

const { common, primary } = theme.palette;

const value = {
  'color-black': common.black,
  'color-primary': primary.main,
};

const generatedStyle = () => {
  let result = '';
  const values = new Map(Object.entries(value));
  values.forEach((val, valKey) => {
    result += `.${valKey}{ color: ${val} }`;
  });
  return result;
};

const color = css`
  ${generatedStyle()}
`;

export default color;
