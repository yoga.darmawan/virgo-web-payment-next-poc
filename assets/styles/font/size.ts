import { css } from '@emotion/react';

import theme from '@/assets/styles/theme';

const value = theme.typography.size;

const generatedStyle = () => {
  let result = '';
  const values = new Map(Object.entries(value));
  values.forEach((val, valKey) => {
    result += `.${valKey}{ font-size: ${val} }`;
  });
  return result;
};

const size = css`
  ${generatedStyle()}
`;

export default size;
