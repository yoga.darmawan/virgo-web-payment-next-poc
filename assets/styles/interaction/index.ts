import { css } from '@emotion/react';

import CursorStyle from './cursor';

export default css`
  ${CursorStyle}
`;
