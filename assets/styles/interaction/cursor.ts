import { css } from '@emotion/react';

const Style = css`
  .pointer {
    cursor: pointer;
  }
`;

export default Style;
