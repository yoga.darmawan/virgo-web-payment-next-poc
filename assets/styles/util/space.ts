export const parseSpace = (spacesArr: number[][]) => {
  if (spacesArr) {
    return spacesArr.map((spaces) => {
      const length = spaces.length;
      let result = '';
      spaces.forEach((space, key) => {
        if (key + 1 !== length) {
          result += `${space}px `;
        } else {
          result += `${space}px`;
        }
      });
      return result;
    });
  }
  return null;
};
