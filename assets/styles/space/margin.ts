import { css } from '@emotion/react';

import theme from '@/assets/styles/theme';

const value = theme.space.margin;

const positions = ['', '-top', '-bottom', '-right', '-left'];

const generatedStyle = () => {
  let result = '';
  const values = new Map(Object.entries(value));
  values.forEach((val, valKey) => {
    positions.forEach((pos) => {
      result += `.${valKey}${pos}{ margin${pos}: ${val} }`;
    });
  });
  return result;
};

const margin = css`
  ${generatedStyle()}
`;

export default margin;
