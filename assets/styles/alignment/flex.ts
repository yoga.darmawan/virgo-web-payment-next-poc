import { css } from '@emotion/react';

const Style = css`
  .flex {
    display: flex;
  }
  .align-center {
    align-items: center;
  }
  .space-between {
    justify-content: space-between;
  }
`;

export default Style;
