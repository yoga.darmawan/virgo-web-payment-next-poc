import { css } from '@emotion/react';

import FlexStyle from './flex';

export default css`
  ${FlexStyle}
`;
