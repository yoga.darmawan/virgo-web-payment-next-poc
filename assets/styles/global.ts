import { css } from '@emotion/react';

import { Theme } from './theme/type';

const Style = (props: Theme) => {
  const { typography, palette } = props;
  return css`
    @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 200;
      src: local('Avenir Book'), local('Avenir-Book'),
        url('../fonts/avenir/Avenir-Book.woff2') format('woff2');
    }
    @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 400;
      src: local('Avenir Medium'), local('Avenir-Medium'),
        url('../fonts/avenir/Avenir-Medium.woff2') format('woff2');
    }
    @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 700;
      src: local('Avenir Black'), local('Avenir-Black'),
        url('../fonts/avenir/Avenir-Black.woff2') format('woff2');
    }

    html {
      font-family: 'Avenir' !important;
      font-size: ${typography.htmlFontSize}px;
      color: ${palette.common.black};
    }
  `;
};

export default Style;
