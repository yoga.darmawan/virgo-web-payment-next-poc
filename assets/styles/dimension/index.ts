import { css } from '@emotion/react';

import WidthStyle from './width';

export default css`
  ${WidthStyle}
`;
