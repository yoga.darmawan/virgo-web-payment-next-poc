import { css } from '@emotion/react';

import theme from '@/assets/styles/theme';

const value = theme.dimension.width;

const generatedStyle = () => {
  let result = '';
  const values = new Map(Object.entries(value));
  values.forEach((val, valKey) => {
    result += `.${valKey}{ width: ${val} }`;
  });
  return result;
};

const width = css`
  ${generatedStyle()}
`;

export default width;
