const typography = {
  htmlFontSize: 16,
  fontFamily: 'Avenir',
  size: {
    size75: '0.75rem',
    size80: '0.80rem',
    size90: '0.90rem',
    size150: '1.5rem',
  },
  weight: {
    weight300: '300',
    weight400: '400',
    weight500: '500',
    weight600: '600',
    weight700: '700',
    weight900: '900',
  },
  h1: {
    fontFamily: 'Avenir',
    fontWeight: 900,
    fontSize: '2rem',
    lineHeight: '3rem',
  },
  h2: {
    fontFamily: 'Avenir',
    fontWeight: 900,
    fontSize: '1.5rem',
    lineHeight: '2.25rem',
  },
  h3: {
    fontFamily: 'Avenir',
    fontWeight: 900,
    fontSize: '1.125rem',
    lineHeight: '1.688rem',
  },
  titleBlack: {
    fontFamily: 'Avenir',
    fontWeight: 900,
    fontSize: '1rem',
    lineHeight: '1.5rem',
  },
  titleMedium: {
    fontFamily: 'Avenir',
    fontWeight: 500,
    fontSize: '1rem',
    lineHeight: '1.5rem',
  },
  contentBlack: {
    fontFamily: 'Avenir',
    fontWeight: 900,
    fontSize: '0.875rem',
    lineHeight: '1.313rem',
  },
  contentMedium: {
    fontFamily: 'Avenir',
    fontWeight: 500,
    fontSize: '0.875rem',
    lineHeight: '1.313rem',
  },
  contentBook: {
    fontFamily: 'Avenir',
    fontWeight: 400,
    fontSize: '0.875rem',
    lineHeight: '1.313rem',
  },
  smallBlack: {
    fontFamily: 'Avenir',
    fontWeight: 900,
    fontSize: '0.75rem',
    lineHeight: '1.125rem',
  },
  smallMedium: {
    fontFamily: 'Avenir',
    fontWeight: 500,
    fontSize: '0.75rem',
    lineHeight: '1.125rem',
  },
};

export default typography;
