import { createTheme } from '@mui/material/styles';

import palette from './palette';
import customComponents from './custom-components';
import space from './space';
import typography from './typography';
import dimension from './dimension';

const theme = createTheme({
  palette,
  customComponents,
  space,
  typography,
  dimension,
});

export default theme;
