import { DimensionOptions } from './type';

const dimension: DimensionOptions = {
  width: {
    w1: '100%',
    w100: '1rem',
    w150: '1.5rem',
  },
};

export default dimension;
