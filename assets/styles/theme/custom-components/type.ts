export interface CustomComponentsOptions {
  modal?: {
    maxWidth: number[];
  };
  topRibbon?: {
    height: number[];
    padding: number[][];
    checkWidth: number[];
    zIndex: number;
  };
}

export interface CustomComponents {
  modal: {
    maxWidth: number[];
  };
  topRibbon: {
    height: number[];
    padding: number[][];
    checkWidth: number[];
    zIndex: number;
  };
}
