import { CustomComponentsOptions } from './type';

const customComponents: CustomComponentsOptions = {
  modal: {
    maxWidth: [600],
  },
  topRibbon: {
    height: [52, 70],
    padding: [
      [0, 20, 0, 20],
      [0, 60, 0, 60],
    ],
    checkWidth: [105, 140],
    zIndex: 100,
  },
};

export default customComponents;
