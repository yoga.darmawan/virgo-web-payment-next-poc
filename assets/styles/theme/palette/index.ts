import { PaletteOptions } from './type';

const palette: PaletteOptions = {
  common: {
    white: '#ffffff',
    black: '#000000',
  },

  primary: {
    main: '#19b1a1',
    light: '#93d6ce',
    lighter: '#caf7f2',
    dark: '#008f7f',
    contrastText: '#ffffff',
  },

  cactus: {
    main: '#21bf73',
    light: '#d2f2e3',
    dark: '#008f7f',
    contrastText: '#ffffff',
  },

  mist: {
    main: '#bdbdbd',
    light: '#eff0f2',
    dark: '#515353',
    contrastText: '#ffffff',
  },

  ashpalth: {
    main: '#5c6277',
    light: '#5c6277',
    dark: '#1c1c1e',
    contrastText: '#ffffff',
  },
};

export default palette;
