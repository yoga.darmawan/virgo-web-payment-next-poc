/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['firebasestorage.googleapis.com', 'www.fintechfutures.com', 'statik.tempo.co'],
    formats: ['image/avif', 'image/webp'],
  },
  env: {
    ENV: process.env.ENV,
    FIREBASE_CONFIG: process.env.FIREBASE_CONFIG,
    SESSION_KEY: process.env.SESSION_KEY,
    TRANSACTION_BASE_API_URL: process.env.TRANSACTION_BASE_API_URL,
    LOCAL_BASE_URL: process.env.LOCAL_BASE_URL,
  },
  publicRuntimeConfig: {
    ASSETS_PREFIX_PATH: '',
  },
};

module.exports = nextConfig;
