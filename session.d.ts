import { Session } from '@/domains/auth/entity';

declare module 'iron-session' {
  interface IronSessionData extends Session {}
}

declare module 'http' {
  interface IncomingMessage {
    body: {
      token: string;
    };
  }
}
