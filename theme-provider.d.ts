import '@emotion/react';

import { Theme as CustomTheme } from '@/assets/styles/theme/type';

declare module '@emotion/react' {
  export interface Theme extends CustomTheme {}
}
