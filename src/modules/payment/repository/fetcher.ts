import { Fetcher, FetcherHelper } from '@/core/collection';
// import { authStorage } from "@helpers/storage";

const { createAxios, clientErrorHandler, clientSuccessHandler } = FetcherHelper;

export default class PaymentFetcher extends Fetcher {
  constructor() {
    super();
    // this.setFetcher(createAxios(process.env.NEXT_PUBLIC_AUTH_API).instance);
    this.setFetcher(createAxios('').instance);
    this.setSuccessHandler(clientSuccessHandler);
    this.setErrorHandler(clientErrorHandler);
  }

  async verifyPartnerPaymentPasscode(data: any) {
    this.setFetcher(createAxios('').instance);
    this.setData(data);
    console.log('fetcher verifyPartnerPaymentPasscode');
    this.setUrl('/api/partner-payment/passcode-verify');
    const response = await this.post();
    return response;
  }

  async requestPaymentNonce(data: any, token: string) {
    this.setFetcher(createAxios(process.env.TRANSACTION_BASE_API_URL || '', token).instance);
    this.setData(data);
    console.log('fetcher requestPaymentNonce');
    this.setUrl('/r/transaction/payment-nonce');
    const response = await this.post();
    return response;
  }

  // async logout() {
  //   this.setFetcher(
  //     createFetch(process.env.NEXT_PUBLIC_AUTH_API, authStorage.getToken())
  //       .instance
  //   );
  //   this.setUrl("/logout");
  //   const response = await this.get();
  //   return response;
  // }
}
