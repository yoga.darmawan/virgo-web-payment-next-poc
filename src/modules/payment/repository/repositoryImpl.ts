import { PaymentNonce, PaymentNonceResponse } from '@/domains/payment/entity';
import PaymentRepository from '@/domains/payment/repository';
import PaymentFetcher from './fetcher';

export default class PaymentRepositoryImpl implements PaymentRepository {
  private fetcher: PaymentFetcher;

  constructor(fetcher: PaymentFetcher) {
    this.fetcher = fetcher;
  }

  async verifyPartnerPaymentPasscode(payload: PaymentNonce): Promise<PaymentNonceResponse> {
    const response = await this.fetcher.verifyPartnerPaymentPasscode(payload);
    return response;
  }

  async requestPaymentNonce(payload: PaymentNonce, token: string): Promise<PaymentNonceResponse> {
    const response = await this.fetcher.requestPaymentNonce(payload, token);
    return response;
  }
}
