import PaymentUseCaseImpl from '../usecase/usecaseImpl';
import PaymentRepositoryImpl from '../repository/repositoryImpl';
import PaymentFetcher from '../repository/fetcher';
// import PaymentViewModel from "../presentation/view-model"

export const fetcher = new PaymentFetcher();
export const repository = new PaymentRepositoryImpl(fetcher);
export const usecase = new PaymentUseCaseImpl(repository);
// export const paymentViewModel = PaymentViewModel
