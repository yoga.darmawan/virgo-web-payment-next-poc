import { PaymentNonce, PaymentNonceResponse } from '@/domains/payment/entity';
import PaymentRepository from '@/domains/payment/repository';
import PaymentUseCase from '@/domains/payment/usecase';

export default class PaymentUseCaseImpl implements PaymentUseCase {
  private repository: PaymentRepository;

  constructor(repository: PaymentRepository) {
    this.repository = repository;
  }

  async verifyPartnerPaymentPasscode(payload: PaymentNonce): Promise<PaymentNonceResponse> {
    const response: PaymentNonceResponse = await this.repository.verifyPartnerPaymentPasscode(
      payload,
    );
    return response;
  }
}
