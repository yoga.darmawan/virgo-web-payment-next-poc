import { createSlice } from '@reduxjs/toolkit';

const initPayment = {
  topupChannels: [],
};

const paymentSlice = createSlice({
  name: 'payment',
  initialState: {
    ...initPayment,
  },
  reducers: {
    // setTopupChannels(state, { payload }) {
    //   state.topupChannels = payload;
    // },
  },
});

export const paymentActions = paymentSlice.actions;

export default paymentSlice;
