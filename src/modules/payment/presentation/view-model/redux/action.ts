import { useDispatch } from 'react-redux';

import { AppThunk } from '@/shared/utils/redux/store';
import Helper from '@/shared/utils/redux/helper';

import { usecase } from '@/modules/payment/util';
import { PaymentNonce } from '@/domains/payment/entity';
// import { paymentActions } from './slice';

export default function ActionModel() {
  const dispatch = useDispatch();

  const helper = Helper();
  // const { showOverlayLoading, hideOverlayLoading } = helper;

  function verifyPartnerPaymentPasscode(
    payload: PaymentNonce,
    successCb?: (response: any) => void,
  ): AppThunk {
    return async () => {
      // showOverlayLoading();
      try {
        const response = await usecase.verifyPartnerPaymentPasscode(payload);
        console.log('action verifyPartnerPaymentPasscode response', response);
        if (successCb) {
          successCb(response);
        }
        // dispatch(paymentActions.setTopupChannels(response.data?.topup_channels));
      } catch (error) {
        console.log(error);
        // dispatch(paymentActions.setTopupChannels([]));
      } finally {
        // hideOverlayLoading();
      }
    };
  }

  return {
    verifyPartnerPaymentPasscode: (payload: PaymentNonce, successCb: (response: any) => void) =>
      dispatch(verifyPartnerPaymentPasscode(payload, successCb)),
    ...helper,
  };
}
