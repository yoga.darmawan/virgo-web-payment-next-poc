import { injectReducer } from '@/shared/utils/redux/store';
import paymentSlice from './redux/slice';
import dataModel from './redux/data';
import actionModel from './redux/action';

export default function MimoViewModel() {
  injectReducer('payment', paymentSlice.reducer);
  return {
    dataModel: () => dataModel(),
    actionModel: () => actionModel(),
  };
}
