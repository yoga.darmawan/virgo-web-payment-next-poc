import React from 'react';
import { useRouter as useNextRouter } from 'next/router';

import Meta from '@/shared/components/meta';
import PaymentViewModel from '@/modules/payment/presentation/view-model';
import { PartnerPaymentQuery } from '@/domains/payment/entity';
import { useRouter } from '@/shared/utils/router/redirect';
import metadata from './meta';

const VerifyPasscode = () => {
  // INIT VIEW MODEL
  const payment = (() => PaymentViewModel())();
  const { actionModel } = payment;
  const action = actionModel();
  // CUSTOM HOOKS
  const { query } = useNextRouter();
  const { redirect } = useRouter();
  const { payment_info: paymentInfo, callback_url: callback } = query as PartnerPaymentQuery;

  const onSuccess = (response: any) => {
    redirect({
      name: 'PartnerPaymentSuccess',
      query: {
        payment_nonce: response.payment_nonce,
        callback_url: callback || '',
      },
    });
  };

  const onVerifyPasscode = () => {
    if (paymentInfo && callback) {
      action.verifyPartnerPaymentPasscode(
        {
          payment_info: paymentInfo,
          callback,
          passcode: '111111',
        },
        onSuccess,
      );
    }
  };
  return (
    <>
      <Meta metadata={metadata} />
      <p>halaman payment entry point payment at partner</p>
      {paymentInfo && callback ? (
        <button type="button" onClick={() => onVerifyPasscode()}>
          confirm passcode to complete payment
        </button>
      ) : null}
    </>
  );
};

export default VerifyPasscode;
