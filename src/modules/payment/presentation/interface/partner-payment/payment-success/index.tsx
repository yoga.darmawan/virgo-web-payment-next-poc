import React from 'react';
import { useRouter as useNextRouter } from 'next/router';

import Meta from '@/shared/components/meta';
// import PaymentViewModel from '@/modules/payment/presentation/view-model';
// import { PartnerPaymentQuery } from '@/domains/payment/entity';
import { useRouter } from '@/shared/utils/router/redirect';
import metadata from './meta';

const VerifyPasscode = () => {
  // INIT VIEW MODEL
  // const payment = (() => PaymentViewModel())();
  // const { actionModel } = payment;
  // const action = actionModel();
  // CUSTOM HOOKS
  const { query } = useNextRouter();
  const { locationRedirect } = useRouter();
  // console.log(query);
  const onBackToMerchant = () => {
    locationRedirect(query.callback_url, {
      payment_nonce: query.payment_nonce,
    });
  };
  return (
    <>
      <Meta metadata={metadata} />
      <p>halaman payment success payment at partner</p>
      <button type="button" onClick={() => onBackToMerchant()}>
        confirm to redirect back to merchant
      </button>
    </>
  );
};

export default VerifyPasscode;
