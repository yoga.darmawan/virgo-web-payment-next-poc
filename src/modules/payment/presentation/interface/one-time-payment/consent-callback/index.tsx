import React from 'react';

import Meta from '@/shared/components/meta';
import { useRouter } from '@/shared/utils/router/redirect';
import AuthViewModel from '@/modules/auth/presentation/view-model';
import metadata from './meta';
import { LoginResponse } from '@/domains/auth/entity';

const VerifyPasscode = () => {
  // INIT VIEW MODEL
  const auth = (() => AuthViewModel())();
  const { actionModel } = auth;
  const action = actionModel();
  // CUSTOM HOOKS
  const { locationRedirect } = useRouter();
  const onSuccess = (response: LoginResponse) => {
    locationRedirect(response.redirect_url);
  };
  React.useEffect(() => {
    const hash = window.location.hash.substr(1);
    const search = new URLSearchParams(hash);
    action.oneTimePaymentLogin(
      {
        token: search.get('access_token') || '',
        payment_auth: search.get('state') || '',
      },
      onSuccess,
    );
  }, []);
  return (
    <>
      <Meta metadata={metadata} />
      <p>halaman consent callback one time payment</p>
      <p>Loading... will be redirected to payment confirmation page</p>
    </>
  );
};

export default VerifyPasscode;
