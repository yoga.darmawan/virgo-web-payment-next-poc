import React from 'react';
import { useRouter as useNextRouter } from 'next/router';

import Meta from '@/shared/components/meta';
import { useRouter } from '@/shared/utils/router/redirect';
import metadata from './meta';

const VerifyPasscode = () => {
  // INIT VIEW MODEL
  // const payment = (() => PaymentViewModel())();
  // const { actionModel } = payment;
  // const action = actionModel();
  // CUSTOM HOOKS
  const { redirect } = useRouter();
  const { query } = useNextRouter();
  const onRedirect = () => {
    redirect({
      name: 'OneTimePaymentPasscode',
      id: query.id as string,
    });
  };
  return (
    <>
      <Meta metadata={metadata} />
      <p>halaman payment detail confirmation one time payment</p>
      <button type="button" onClick={() => onRedirect()}>
        Click here to authorize payment
      </button>
    </>
  );
};

export default VerifyPasscode;
