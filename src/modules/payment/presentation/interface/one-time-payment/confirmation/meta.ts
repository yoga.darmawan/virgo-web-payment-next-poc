import { Meta } from '@/shared/components/meta';

const meta: Meta[] = [
  {
    name: 'title',
    content: 'One Time Payment Confirmation | Virgoku',
  },
  {
    name: 'description',
    content:
      'Wujudkan impian besar dari langkah kecil bersama virgo. Top Up Kembalian kamu untuk jadi Saldo Virgo',
  },
];

export default meta;
