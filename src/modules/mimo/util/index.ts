import MimoUseCaseImpl from '../usecase/usecaseimpl';
import MimoRepositoryImpl from '../repository/repositoryimpl';

export const repository = new MimoRepositoryImpl();
export const usecase = new MimoUseCaseImpl(repository);
