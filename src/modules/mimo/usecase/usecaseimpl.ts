import { TopupChannelsResponse } from '@/domains/mimo/entity';
import MimoUseCase from '@/domains/mimo/usecase';
import MimoRepository from '@/domains/mimo/repository';

export default class MimoRepositoryImpl implements MimoUseCase {
  private repository: MimoRepository;

  constructor(repository: MimoRepository) {
    this.repository = repository;
  }

  async viewTopupPage(): Promise<TopupChannelsResponse> {
    try {
      const response = await this.repository.getTopupChannels();
      return response;
    } catch (error) {
      throw error;
    }
  }
}
