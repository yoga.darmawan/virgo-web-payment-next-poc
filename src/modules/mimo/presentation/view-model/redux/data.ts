import { useSelector } from 'react-redux';

import { RootState } from '@/shared/utils/redux/store';

export default function DataModel() {
  const mimo = useSelector((state: RootState) => state.mimo);

  const topupChannels = useSelector((state: RootState) => state.mimo.topupChannels);

  return {
    mimo,
    topupChannels,
  };
}
