import { createSlice } from '@reduxjs/toolkit';

const initMimo = {
  topupChannels: [],
};

const mimoSlice = createSlice({
  name: 'mimo',
  initialState: {
    ...initMimo,
  },
  reducers: {
    setTopupChannels(state, { payload }) {
      state.topupChannels = payload;
    },
  },
});

export const mimoActions = mimoSlice.actions;

export default mimoSlice;
