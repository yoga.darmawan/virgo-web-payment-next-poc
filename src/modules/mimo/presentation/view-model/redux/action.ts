import { useDispatch } from 'react-redux';

import { AppThunk } from '@/shared/utils/redux/store';
import Helper from '@/shared/utils/redux/helper';

import { TopupChannelsResponse } from '@/domains/mimo/entity';
import { usecase } from '@/modules/mimo/util';
import { mimoActions } from './slice';

export default function ActionModel() {
  const dispatch = useDispatch();

  const helper = Helper();
  const { showOverlayLoading, hideOverlayLoading } = helper;

  function viewTopupPage(): AppThunk {
    return async () => {
      showOverlayLoading();
      try {
        const response: TopupChannelsResponse = await usecase.viewTopupPage();
        dispatch(mimoActions.setTopupChannels(response.data?.topup_channels));
      } catch (error) {
        dispatch(mimoActions.setTopupChannels([]));
      } finally {
        hideOverlayLoading();
      }
    };
  }

  return {
    viewTopupPage: () => dispatch(viewTopupPage()),
    ...helper,
  };
}
