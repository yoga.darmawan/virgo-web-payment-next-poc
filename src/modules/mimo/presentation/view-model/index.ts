import { injectReducer } from '@/shared/utils/redux/store';
import mimoSlice from './redux/slice';
import dataModel from './redux/data';
import actionModel from './redux/action';

export default function MimoViewModel() {
  injectReducer('mimo', mimoSlice.reducer);
  return {
    dataModel: () => dataModel(),
    actionModel: () => actionModel(),
  };
}
