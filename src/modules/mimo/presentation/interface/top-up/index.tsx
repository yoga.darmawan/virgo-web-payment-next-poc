import React from 'react';
import { useRouter } from 'next/router';
import { useTheme } from '@emotion/react';

import MimoViewModel from '@/modules/mimo/presentation/view-model';

import Meta from '@/shared/components/meta';
import metadata from './meta';
import SummaryCard from './summary-card';
import TopupGroup from './group';

import { RootStyle } from './style';
import { Theme } from '@/assets/styles/theme/type';

interface Query {
  phone?: string;
  balance?: string;
  max_topup?: string;
  max_balance?: string;
  max_money_in_monthly?: string;
}

const setBodyColor = ({ palette: { mist, common }, breakpoints: { values: bp } }: Theme) => {
  const width = window.innerWidth;
  if (width <= bp.sm) {
    document.body.style.backgroundColor = mist.light;
  } else {
    document.body.style.backgroundColor = common.white;
  }
};

const TopUp = () => {
  // MIMO
  const mimo = (() => MimoViewModel())();
  const { actionModel, dataModel } = mimo;
  const action = actionModel();
  const { topupChannels: channels } = dataModel();
  // HOOKS
  const theme = useTheme();
  const { query } = useRouter();
  React.useEffect(() => {
    setBodyColor(theme);
    window.addEventListener('resize', () => setBodyColor(theme));
    action.viewTopupPage();
  }, []);
  // DATA
  const {
    phone,
    balance,
    max_topup: maxTopup,
    max_balance: maxBalance,
    max_money_in_monthly: maxMoneyInMonthly,
  }: Query = query;

  let completeInfo = false;
  if (phone && balance && maxTopup && maxBalance && maxMoneyInMonthly) {
    completeInfo = true;
  }
  return (
    <>
      <Meta metadata={metadata} />
      <RootStyle>
        <SummaryCard channels={channels} completeInfo={completeInfo} />
        <TopupGroup channels={channels} />
      </RootStyle>
    </>
  );
};

export default TopUp;
