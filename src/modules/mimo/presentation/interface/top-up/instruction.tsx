import React from 'react';

import { TopupChannel } from '@/domains/mimo/entity';
import ModalBottomSheet from '@/shared/components/modal-bottom-sheet';

import { InstructionStyle } from './style';

interface Props {
  open: boolean;
  setOpen: (isOpen: boolean) => any;
  channel: TopupChannel;
}

const Instruction = (props: { channel: TopupChannel }) => {
  const { channel } = props;
  return (
    <div>
      <div className="instruction_title weight900 m100-bottom">{channel.channel_name}</div>
      <div>
        {channel.instructions &&
          channel.instructions.map((inst, key) => (
            <div key={inst} className="instruction_row flex align-center">
              <div className="instruction_row-number size80 m50-right">{key + 1}</div>
              <div className="size90">{inst}</div>
            </div>
          ))}
      </div>
      <div className="p100">
        <div className="instruction_footnote">
          <div className="size80 m50-bottom weight900">{channel?.footer_notes?.label}</div>
          <div className="size80" style={{ whiteSpace: 'pre-wrap' }}>
            {channel?.footer_notes?.value}
          </div>
        </div>
      </div>
    </div>
  );
};

const InstructionWrapper = ({ open, setOpen, channel }: Props) => {
  return (
    <>
      <ModalBottomSheet open={open} onOpen={setOpen}>
        <InstructionStyle>
          <Instruction channel={channel} />
        </InstructionStyle>
      </ModalBottomSheet>
    </>
  );
};

export default InstructionWrapper;
