import React from 'react';
import { useRouter } from 'next/router';
import Snackbar from '@mui/material/Snackbar';
import { useTheme } from '@emotion/react';

import { TopupChannel } from '@/domains/mimo/entity';
import ModalBottomSheet from '@/shared/components/modal-bottom-sheet';
import { idrMask } from '@/shared/utils/string/currency';
import { cdnSubPath } from '@/shared/utils/string/path';

import { SummaryCardStyle } from './style';

interface Props {
  channels: TopupChannel[];
  completeInfo: boolean;
}

const SummaryCard = ({ channels, completeInfo }: Props) => {
  // HOOKS
  const [openMaxInfo, setOpenMaxInfo] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const theme = useTheme();
  const { cactus, common } = theme.palette;
  // QUERY PARAMS
  const { query, back } = useRouter();
  const {
    phone,
    balance,
    group_name: name,
    group_id: id,
    max_topup: maxTopup,
    max_balance: maxBalance,
    max_money_in_monthly: maxMoneyInMonthly,
  } = query;
  const isGroupDetailPage = name !== undefined && id !== undefined;
  // MEMOIZED VALUE
  const channel = React.useMemo(() => {
    const index = channels.findIndex((chan) => chan.group_id === id && chan.group_name === name);
    if (index > -1) {
      return channels[index];
    }
    return null;
  }, [channels, id, name]);

  const va = React.useMemo(() => {
    const vaTemplate = channel?.copiable_data?.value;
    if (completeInfo && phone && vaTemplate) {
      return vaTemplate.replace('<phonenumber>', phone as string);
    }
    if (vaTemplate) {
      return vaTemplate.replace(
        '<phonenumber>',
        vaTemplate === '<phonenumber>' ? 'no. ponsel' : ' + no. ponsel',
      );
    } else {
      return '-';
    }
  }, [channel, completeInfo, phone]);

  const vaExample = React.useMemo(() => {
    const example = '0812121212';
    const vaTemplate = channel?.copiable_data?.value;
    if (vaTemplate) {
      return vaTemplate.replace('<phonenumber>', example);
    } else {
      return '-';
    }
  }, [channel]);
  // SUMMARY CARD
  const onCopyVa = (value: string) => {
    if (navigator) {
      navigator.clipboard.writeText(value);
      setOpenSnackbar(true);
    }
  };
  const summary = React.useMemo(() => {
    const card = [];
    const maxTopupInfo = (
      <div className="max-topup-info size75">
        <div>
          <span>Maksimal Saldo Top Up : </span>
          <span className="weight900">
            {completeInfo && maxTopup ? idrMask(Number(maxTopup), 0) : '-'}
          </span>
        </div>
        <div className="pointer" onClick={() => setOpenMaxInfo(true)}>
          <img src={`${cdnSubPath()}/images/info-circle-green.png`} alt="info" />
        </div>
        <ModalBottomSheet open={openMaxInfo} onOpen={setOpenMaxInfo}>
          <div className="p100">
            <div className="size80 m50-bottom">
              Maks. saldo: <span className="weight900"> {idrMask(Number(maxBalance), 0)}</span>
            </div>
            <div className="size80">
              Maks. uang masuk per bulan:
              <span className="weight900"> {idrMask(Number(maxMoneyInMonthly), 0)}</span>
            </div>
          </div>
        </ModalBottomSheet>
      </div>
    );
    if (isGroupDetailPage) {
      let lowerInfo = (
        <span className="size75">
          <span>Contoh : </span>
          <span className="weight900 color-black">{vaExample}</span>
        </span>
      );
      if (completeInfo && phone) {
        lowerInfo = maxTopupInfo;
      }
      card.push(
        <div key="summary-card1" className="balance-card">
          <div className="balance-wrapper p100-bottom">
            <div className="flex align-center m100-bottom">
              <span className="size90">{channel?.copiable_data?.label}</span>
            </div>
            <div className="flex align-center space-between w1">
              <span id="va-number" className="size150 weight900 color-primary">
                {va}
              </span>
              {completeInfo && phone && (
                <>
                  <div className="pointer" onClick={() => onCopyVa(va)}>
                    <img src={`${cdnSubPath()}/images/copy-green.png`} alt="copy" />
                  </div>
                  <Snackbar
                    open={openSnackbar}
                    autoHideDuration={3000}
                    onClose={() => setOpenSnackbar(false)}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'center',
                    }}
                    message="Nomor virtual account tersalin"
                    ContentProps={{
                      sx: {
                        backgroundColor: cactus.main,
                        color: common.white,
                      },
                    }}
                  />
                </>
              )}
            </div>
          </div>
          <div className="p100-top">{lowerInfo}</div>
        </div>,
      );
    } else if (!isGroupDetailPage) {
      card.push(
        <div key="summary-card2" className="info">
          <div className="info-text">
            <span>Cara top up Virgo</span>
            <span>Udah tau belum.. Kamu bisa melakukan topup diberbagai Bank & Partner Virgo</span>
          </div>
          <div className="hand-with-money">
            <img
              src={`${cdnSubPath()}/images/hand-with-money.png`}
              width={120}
              height={107}
              alt="hand with money"
            />
          </div>
        </div>,
      );
      if (completeInfo && balance) {
        card.push(
          <div key="summary-card3" className="balance-card">
            <div className="balance-wrapper p100-bottom">
              <div className="flex align-center m100-bottom">
                <span className="size90 m50-right">Saldo</span>
                <div>
                  <img
                    src={`${cdnSubPath()}/images/virgo-full.png`}
                    width={36}
                    height={19}
                    alt="virgo"
                  />
                </div>
              </div>
              <span className="size150 weight900">
                {completeInfo && balance ? idrMask(Number(balance), 0) : '-'}
              </span>
            </div>
            <div className="p100-top">{maxTopupInfo}</div>
          </div>,
        );
      }
    }
    return card;
  }, [
    isGroupDetailPage,
    channel,
    va,
    vaExample,
    balance,
    completeInfo,
    maxTopup,
    phone,
    maxBalance,
    maxMoneyInMonthly,
    openSnackbar,
    openMaxInfo,
  ]);

  return (
    <SummaryCardStyle>
      <div className="m100-bottom">
        <img
          className="pointer"
          onClick={() => back()}
          src={`${cdnSubPath()}/images/arrow-left.png`}
          alt="arrow left"
        />
        <span className="title">{isGroupDetailPage && name ? name : 'Top Up Virgo'}</span>
      </div>
      {summary}
    </SummaryCardStyle>
  );
};

export default React.memo(SummaryCard);
