import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { TopupChannel } from '@/domains/mimo/entity';
import Instruction from './instruction';
import { cdnSubPath, urlPath } from '@/shared/utils/string/path';

import { GroupStyle } from './style';

interface Props {
  channels: TopupChannel[];
}

const Group = ({ channels }: Props) => {
  // STATE
  const [open, setOpen] = React.useState<boolean>(false);
  const [channel, setChannel] = React.useState<TopupChannel>({});
  // DATA
  const { query } = useRouter();
  const { group_name: name, group_id: id } = query;
  const isGroupDetailPage = name !== undefined && id !== undefined;

  const groupTypes = React.useMemo(() => {
    const result: string[] = [];
    for (let index = 0; index < channels.length; index++) {
      const channel = channels[index];
      const resultIndex = result.findIndex((type) => type === channel.group_type);
      if (resultIndex === -1 && channel.group_type) {
        result.push(channel.group_type);
      }
    }
    return result;
  }, [channels]);
  // CALLBACK
  const getGroupInfos = React.useCallback(
    (groupType) => {
      const result: {
        group_name: string;
        group_logo_url: string;
        group_id: string;
      }[] = [];
      const filtered = channels.filter((channel) => channel.group_type === groupType);
      for (let index = 0; index < filtered.length; index++) {
        const channel = filtered[index];
        const resultIndex = result.findIndex((res) => res.group_name === channel.group_name);
        if (
          resultIndex === -1 &&
          channel.group_name !== undefined &&
          channel.group_logo_url !== undefined &&
          channel.group_id !== undefined
        ) {
          result.push({
            group_name: channel.group_name,
            group_logo_url: channel.group_logo_url,
            group_id: channel.group_id,
          });
        }
      }
      return result;
    },
    [channels],
  );

  const groupChannels = React.useMemo(() => {
    return channels.filter((channel) => channel.group_name === name && channel.group_id === id);
  }, [channels, name, id]);

  const onPickChannel = (chan: TopupChannel) => {
    setOpen(true);
    setChannel(chan);
  };

  return (
    <GroupStyle>
      <Instruction open={open} setOpen={setOpen} channel={channel} />
      {!isGroupDetailPage ? (
        groupTypes.map((groupType) => (
          <div key={groupType} className="group-card m150-bottom">
            <div className="p100">
              <span className="size90 weight900">{groupType}</span>
            </div>
            {getGroupInfos(groupType).map((groupInfo) => (
              <Link
                key={groupInfo.group_name}
                passHref
                href={{
                  pathname: `${urlPath('/top-up')}`,
                  query: {
                    group_name: groupInfo.group_name,
                    group_id: groupInfo.group_id,
                    ...query,
                  },
                }}
              >
                <div className="group-agent">
                  <div className="group-agent_image m150-right">
                    <img src={groupInfo.group_logo_url} width={40} height={30} alt="alfamart" />
                  </div>
                  <span className="size90">{groupInfo.group_name}</span>
                </div>
              </Link>
            ))}
          </div>
        ))
      ) : (
        <div className="group-card m150-bottom">
          <div className="p100">
            <span className="size90 weight900">Cara Top Up</span>
          </div>
          {groupChannels.map((channel) => (
            <div
              key={channel.channel_id}
              className="group-agent"
              onClick={() => onPickChannel(channel)}
            >
              <div className="group-agent_image m150-right">
                <img
                  src={channel.channel_logo_url ? channel.channel_logo_url : ''}
                  width={40}
                  height={30}
                  alt="alfamart"
                />
              </div>
              <div className="w1 flex align-center space-between">
                <span className="size90">{channel.channel_name}</span>
                <div>
                  <img src={`${cdnSubPath()}/images/chevron-down-misty.png`} alt="chevron down" />
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </GroupStyle>
  );
};

export default Group;
