import styled from '@emotion/styled';
import { css } from '@emotion/react';

import { mediaQuery } from '@/assets/styles/util/media-query';

export const RootStyle = styled.div`
  max-width: 490px;
  ${mediaQuery({
    margin: ['0', '4rem auto 0px auto'],
  })}
`;

export const SummaryCardStyle = styled.div`
  ${({
    theme: {
      palette: { common, mist },
    },
  }) => css`
    border-radius: 16px;
    padding: 1.5rem;
    background-color: ${mist.light};
    ${mediaQuery({
      marginBottom: ['0px', '2rem'],
    })}
    .title {
      margin-left: 1rem;
      font-size: 1.15rem;
      font-weight: 900;
    }

    .info {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
    .info-text {
      display: flex;
      flex-direction: column;
      span {
        &:first-child {
          font-weight: 900;
          margin-bottom: 0.5rem;
        }
        &:last-child {
          font-size: 0.8rem;
        }
      }
    }
    .hand-with-money {
      margin-right: -20px;
    }
    .balance-card {
      background-color: ${common.white};
      padding: 1rem 1.5rem;
      border-radius: 16px;
    }
    .balance-wrapper {
      display: flex;
      flex-direction: column;
      border-bottom: 1px solid ${mist.light};
    }
    .max-topup-info {
      color: ${mist.dark};
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
  `}
`;

export const GroupStyle = styled.div`
  ${({
    theme: {
      palette: { common, mist },
    },
  }) => css`
    .group-card {
      background-color: ${common.white};
      border-radius: 14px;
      box-shadow: rgb(0 0 0 / 10%) 0px 4px 12px;
    }
    .group-agent {
      display: flex;
      align-items: center;
      padding: 22px 1rem 22px 80px;
      border-bottom: 1px solid ${mist.light};
      position: relative;
      cursor: pointer;
      &:last-child {
        border-bottom: 0px;
      }
    }
    .group-agent_image {
      position: absolute;
      left: 1rem;
      img {
        object-fit: contain;
      }
    }
  `}
`;

export const InstructionStyle = styled.div`
  ${({
    theme: {
      palette: { primary, mist },
    },
  }) => css`
    ${mediaQuery({
      minHeight: [null, '200px'],
    })}
    .instruction_title {
      padding: 1rem 1rem 0px 1.5rem;
    }
    .instruction_footnote {
      padding: 1rem;
      border-radius: 10px;
      background-color: ${mist.light};
      color: ${mist.dark};
    }
    .instruction_row {
      padding: 0.7rem 1rem;
      border-bottom: 1px solid ${mist.light};
    }
    .instruction_row-number {
      height: 20px;
      min-height: 20px;
      width: 20px;
      min-width: 20px;
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      line-height: 1rem;
      color: $primary;
      background-color: ${primary.lighter};
    }
  `}
`;
