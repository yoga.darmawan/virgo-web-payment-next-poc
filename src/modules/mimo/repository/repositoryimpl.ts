import { TopupChannelsResponse } from '@/domains/mimo/entity';
import MimoRepository from '@/domains/mimo/repository';
import { getRemoteConfig } from '@/shared/utils/firebase/remote-config';

export default class MimoRepositoryImpl implements MimoRepository {
  async getTopupChannels(): Promise<TopupChannelsResponse> {
    try {
      const response = await getRemoteConfig('virgoTopupChannels');
      const TopupChannels = JSON.parse(response);
      return TopupChannels;
    } catch (error) {
      throw error;
    }
  }
}
