import { OneTimePaymentLogin, LoginResponse } from '@/domains/auth/entity';
import AuthRepository from '@/domains/auth/repository';
import AuthFetcher from './fetcher';

export default class AuthRepositoryImpl implements AuthRepository {
  private fetcher: AuthFetcher;

  constructor(fetcher: AuthFetcher) {
    this.fetcher = fetcher;
  }

  async oneTimePaymentLogin(payload: OneTimePaymentLogin): Promise<LoginResponse> {
    const response = await this.fetcher.oneTimePaymentLogin(payload);
    return response;
  }
}
