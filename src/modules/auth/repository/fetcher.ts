import { Fetcher, FetcherHelper } from '@/core/collection';
import { OneTimePaymentLogin, LoginResponse } from '@/domains/auth/entity';
// import { authStorage } from "@helpers/storage";

const { createAxios, clientErrorHandler, clientSuccessHandler } = FetcherHelper;

export default class AuthFetcher extends Fetcher {
  constructor() {
    super();
    // this.setFetcher(createAxios(process.env.NEXT_PUBLIC_AUTH_API).instance);
    this.setFetcher(createAxios('').instance);
    this.setSuccessHandler(clientSuccessHandler);
    this.setErrorHandler(clientErrorHandler);
  }

  async oneTimePaymentLogin(payload: OneTimePaymentLogin): Promise<LoginResponse> {
    this.setFetcher(createAxios(process.env.LOCAL_BASE_URL || '').instance);
    const loginPayload = {
      token: payload.token,
      payment_auth: payload.payment_auth,
    };
    this.setData(loginPayload);
    this.setUrl('/api/login/one-time-payment');
    const response = await this.post();
    return response;
  }
}

// export default withSessionApi(async function handler(req: NextApiRequest, res: NextApiResponse) {
//   try {
//     console.log('API LOG IN URL', req.url);
//     console.log('API LOG IN QUERY', req.query);
//     console.log('API LOG IN BODY', req.body);
//     const {
//       query: { service },
//       body: { token },
//     } = req;
//     // let redirectUrl = null;
//     // if (service === 'partner_payment') {
//     //   req.session.isPartnerPayment = true;
//     // } else if (service === 'one_time_payment') {
//     //   req.session.isOneTimePayment = true;
//     //   redirectUrl = 'http://localhost:4000/one-time-payment/payment-confirmation';
//     // }
//     req.session.token = token;
//     await req.session.save();
//     console.log('session', req.session);
//     res.status(200);
//     res.send({
//       redirect_url: redirectUrl,
//     });
//   } catch (error) {
//     console.log(error);
//     res.status(400);
//     res.send({
//       error_message: 'ini adalah error',
//     });
//   }
// }, 'PartnerPayment');
