import { OneTimePaymentLogin, LoginResponse } from '@/domains/auth/entity';
import AuthRepository from '@/domains/auth/repository';
import AuthUseCase from '@/domains/auth/usecase';

export default class AuthUseCaseImpl implements AuthUseCase {
  private repository: AuthRepository;

  constructor(repository: AuthRepository) {
    this.repository = repository;
  }

  async oneTimePaymentLogin(payload: OneTimePaymentLogin): Promise<LoginResponse> {
    const response: LoginResponse = await this.repository.oneTimePaymentLogin(payload);
    return response;
  }
}
