import { injectReducer } from '@/shared/utils/redux/store';
import authSlice from './redux/slice';
import dataModel from './redux/data';
import actionModel from './redux/action';

export default function MimoViewModel() {
  injectReducer('auth', authSlice.reducer);
  return {
    dataModel: () => dataModel(),
    actionModel: () => actionModel(),
  };
}
