import { useDispatch } from 'react-redux';

import { AppThunk } from '@/shared/utils/redux/store';
import Helper from '@/shared/utils/redux/helper';

import { usecase } from '@/modules/auth/util';
import { OneTimePaymentLogin } from '@/domains/auth/entity';
// import { paymentActions } from './slice';

export default function ActionModel() {
  const dispatch = useDispatch();

  const helper = Helper();
  // const { showOverlayLoading, hideOverlayLoading } = helper;

  function oneTimePaymentLogin(
    payload: OneTimePaymentLogin,
    successCb?: (response: any) => void,
  ): AppThunk {
    return async () => {
      // showOverlayLoading();
      try {
        const response = await usecase.oneTimePaymentLogin(payload);
        if (successCb) {
          successCb(response);
        }
        // dispatch(paymentActions.setTopupChannels(response.data?.topup_channels));
      } catch (error) {
        console.log(error);
        // dispatch(paymentActions.setTopupChannels([]));
      } finally {
        // hideOverlayLoading();
      }
    };
  }

  return {
    oneTimePaymentLogin: (payload: OneTimePaymentLogin, successCb: (response: any) => void) =>
      dispatch(oneTimePaymentLogin(payload, successCb)),
    ...helper,
  };
}
