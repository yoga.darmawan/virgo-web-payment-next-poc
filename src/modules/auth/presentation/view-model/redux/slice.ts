import { createSlice } from '@reduxjs/toolkit';

const initPayment = {
  auth: null,
};

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    ...initPayment,
  },
  reducers: {
    // setTopupChannels(state, { payload }) {
    //   state.topupChannels = payload;
    // },
  },
});

export const authActions = authSlice.actions;

export default authSlice;
