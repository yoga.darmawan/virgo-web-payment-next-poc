import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

const uiSlice = createSlice({
  name: 'ui',
  initialState: {
    overlayLoading: false,
  },
  reducers: {
    showOverlayLoading(state) {
      state.overlayLoading = true;
    },
    hideOverlayLoading(state) {
      state.overlayLoading = false;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice;
