import { useSelector } from 'react-redux';

import { RootState } from '@/shared/utils/redux/store';

export default function DataModel() {
  const overlayLoading = useSelector((state: RootState) => state?.ui?.overlayLoading);
  return {
    overlayLoading,
  };
}
