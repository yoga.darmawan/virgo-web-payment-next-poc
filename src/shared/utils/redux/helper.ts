import { useDispatch } from 'react-redux';
import { AppDispatch } from './store';
import { uiActions } from './ui-slice';

export default function Helper() {
  const dispatch: AppDispatch = useDispatch();

  function showOverlayLoading() {
    return dispatch(uiActions.showOverlayLoading());
  }

  function hideOverlayLoading() {
    return dispatch(uiActions.hideOverlayLoading());
  }

  return {
    showOverlayLoading: () => dispatch(showOverlayLoading()),
    hideOverlayLoading,
  };
}
