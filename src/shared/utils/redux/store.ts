import { AnyAction, applyMiddleware } from 'redux';
import thunk, { ThunkAction } from 'redux-thunk';
import { createWrapper } from 'next-redux-wrapper';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { createInjectStore, injectReducer as iR } from 'redux-reducers-injector';

import uiSlice from './ui-slice';

const store = createInjectStore(
  {
    ui: uiSlice.reducer,
  },
  composeWithDevTools(applyMiddleware(thunk)),
);

const makeStore = () => store;
export const wrapper = createWrapper(makeStore);
export const injectReducer = (name: string, reducer: object) => iR(name, reducer);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, AnyAction>;
