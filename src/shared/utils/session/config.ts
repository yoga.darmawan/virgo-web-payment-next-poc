import { IronSessionOptions } from 'iron-session';

interface Config {
  PartnerPayment: IronSessionOptions;
  OneTimePayment: IronSessionOptions;
}

const config: Config = {
  PartnerPayment: {
    cookieName: '__session',
    password: process.env.SESSION_KEY || '',
    // secure: true should be used in production (HTTPS) but can't be used in development (HTTP)
    cookieOptions: {
      httpOnly: true,
      maxAge: undefined,
      // maxAge: 5 * 60, // 5 MINUTES
      secure: process.env.NODE_ENV === 'production',
      sameSite: 'strict',
    },
  },
  OneTimePayment: {
    cookieName: '__session',
    password: process.env.SESSION_KEY || '',
    // secure: true should be used in production (HTTPS) but can't be used in development (HTTP)
    cookieOptions: {
      httpOnly: true,
      maxAge: undefined,
      // maxAge: 60 * 60, // 1 HOUR
      secure: process.env.NODE_ENV === 'production',
      sameSite: 'strict',
    },
  },
};

export type ConfigParam = keyof Config;

export default config;
