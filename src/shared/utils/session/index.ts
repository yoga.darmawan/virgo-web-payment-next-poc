import { NextApiHandler, GetServerSidePropsContext, GetServerSidePropsResult } from 'next';
import { withIronSessionSsr, withIronSessionApiRoute } from 'iron-session/next';

import config, { ConfigParam } from './config';

export function withSessionApi(handler: NextApiHandler, configParam: ConfigParam) {
  return withIronSessionApiRoute(handler, config[configParam]);
}

export function withSessionSsr<P extends { [key: string]: unknown } = { [key: string]: unknown }>(
  handler: (
    context: GetServerSidePropsContext,
  ) => GetServerSidePropsResult<P> | Promise<GetServerSidePropsResult<P>>,
  configParam: ConfigParam,
) {
  return withIronSessionSsr(handler, config[configParam]);
}
