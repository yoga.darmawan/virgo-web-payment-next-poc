export function idrMask(amount: number, decimalCount = 2, decimal = ',', thousands = '.'): string {
  if (!amount) return 'Rp 0';
  const negativeSign = amount < 0 ? '-' : '';
  const i = Number(Math.abs(Number(amount)).toFixed(decimalCount)).toString();
  const j = i.length > 3 ? i.length % 3 : 0;
  const IDR =
    negativeSign +
    (j ? i.substr(0, j) + thousands : '') +
    i.substr(j).replace(/(\d{3})(?=\d)/g, `$1${thousands}`) +
    (decimalCount
      ? decimal +
        Math.abs(amount - Number(i))
          .toFixed(decimalCount)
          .slice(2)
      : '');
  return `Rp ${IDR}`;
}
