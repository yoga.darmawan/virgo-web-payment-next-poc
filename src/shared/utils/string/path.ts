import getConfig from 'next/config';

const {
  publicRuntimeConfig: { ASSETS_PREFIX_PATH },
} = getConfig();
const isProd = process.env.NODE_ENV === 'production';

export const cdnSubPath = (): string => {
  return isProd ? ASSETS_PREFIX_PATH : '';
};

export const urlPath = (path: string): string => {
  return `/static${path}`;
};
