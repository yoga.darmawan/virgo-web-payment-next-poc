const configs = {
  virgoTopupChannels: {
    key: 'virgo_topup_instructions',
    defaultValue: '{}',
  },
};

export type RemoteConfigParam = keyof typeof configs;

export default configs;
