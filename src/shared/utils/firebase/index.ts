import { initializeApp } from 'firebase/app';
import { decode } from 'js-base64';

const configBase64 = process.env.FIREBASE_CONFIG;

export const firebaseConfig = configBase64 ? JSON.parse(decode(configBase64)) : null;

const app = initializeApp(firebaseConfig);

export default app;
