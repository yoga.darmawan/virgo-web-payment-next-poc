import { getRemoteConfig as fetch, getValue, fetchAndActivate } from 'firebase/remote-config';
import app from '@/shared/utils/firebase';
import configs, { RemoteConfigParam } from './remote-config-params';

const env = process.env.ENV;

const remoteConfig = fetch(app);

// 12 hours
let fetchInterval = 43200000;
if (env === 'development' || env === 'staging') {
  fetchInterval = 0;
} else if (env === 'production') {
  // 30 minutes
  fetchInterval = 1800000;
}

if (remoteConfig && remoteConfig.settings && remoteConfig.settings.minimumFetchIntervalMillis) {
  remoteConfig.settings.minimumFetchIntervalMillis = fetchInterval;
}

export const getValueWeb = (param: string) => getValue(remoteConfig, param);

export const getRemoteConfig = async (param: RemoteConfigParam) => {
  try {
    const config = configs[param];
    remoteConfig.defaultConfig = {
      [config.key]: config.defaultValue,
    };
    await fetchAndActivate(remoteConfig);
    const val = getValue(remoteConfig, config.key);
    const result = val.asString();
    return result;
  } catch (err) {
    throw err;
  }
};
