import React from 'react';
import { useRouter as useNextRouter } from 'next/router';

import { createUrlParam } from '@/shared/core/fetcher/helper';

type Name = 'PartnerPaymentSuccess' | 'OneTimePaymentConfirmation' | 'OneTimePaymentPasscode';

interface Params {
  url?: string;
  name?: Name;
  query?: {
    [key: string]: string | number;
  };
  id?: string | number;
}

export const generateUrl = ({ name, id, query }: Params): string => {
  let url = '';
  switch (name) {
    case 'PartnerPaymentSuccess':
      url = `${createUrlParam('/payment-success', query)}`;
      break;
    case 'OneTimePaymentConfirmation':
      url = `/one-time-payment/confirmation/${id}`;
      break;
    case 'OneTimePaymentPasscode':
      url = `/one-time-payment/passcode/${id}`;
      break;
    default:
      break;
  }
  return url;
};

export const useRouter = () => {
  const router = useNextRouter();
  const redirect = React.useCallback(
    ({ name, id, query, url }: Params): void => {
      if (url) {
        router.push(url);
      } else {
        router.push(generateUrl({ name, id, query }));
      }
    },
    [router],
  );
  const locationRedirect = React.useCallback((name, query?: any) => {
    window.location.href = createUrlParam(name, query);
  }, []);
  return {
    redirect,
    locationRedirect,
  };
};
