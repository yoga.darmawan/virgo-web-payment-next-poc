import Head from 'next/head';
import React from 'react';

export interface Meta {
  name?: 'title' | 'viewport' | 'description' | '';
  content?: string;
}

interface Props {
  metadata: Meta[];
}

const Meta = ({ metadata = [] }: Props) => {
  const renderedMeta = ({ name = '', content = '' }: Meta) => {
    if (name === 'title') {
      return <title key={name}>{content}</title>;
    } else if (name === 'description' || name === 'viewport') {
      return <meta key={name} name={name} content={content} />;
    }
    return <></>;
  };
  return (
    <Head>
      <meta data-charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      {metadata.map((data) => renderedMeta(data))}
    </Head>
  );
};

export default Meta;
