import React from 'react';
import Modal from '@mui/material/Modal';
import CircularProgress from '@mui/material/CircularProgress';
import DataModel from '@/shared/utils/redux/data';
import Style from './style';

const OverlayLoading = () => {
  const { overlayLoading } = DataModel();
  return (
    <Modal open={overlayLoading}>
      <Style>
        <CircularProgress sx={{ color: '#19b1a1' }} />
      </Style>
    </Modal>
  );
};

export default React.memo(OverlayLoading);
