import styled from '@emotion/styled';

const Style = styled.div`
  outline: none;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
`;

export default Style;
