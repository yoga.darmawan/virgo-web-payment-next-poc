import styled from '@emotion/styled';
import { css } from '@emotion/react';

import { mediaQuery } from '@/assets/styles/util/media-query';

const Style = styled.div`
  ${({
    theme: {
      customComponents: { topRibbon },
    },
  }) => css`
    .ribbon {
      position: fixed;
      left: 0px;
      top: 0px;
      right: 0px;
      z-index: ${topRibbon.zIndex};
    }
    .content {
      margin: 0 auto;
      ${mediaQuery({
        paddingTop: topRibbon.height,
      })}
    }
  `}
`;

export default Style;
