import React, { ReactElement } from 'react';
import TopRibbon from '@/shared/components/top-ribbon';
import Style from './style';

export default function withTopRibbon(page: ReactElement) {
  return (
    <Style>
      <div className="ribbon">
        <TopRibbon />
      </div>
      <div className="content">{page}</div>
    </Style>
  );
}
