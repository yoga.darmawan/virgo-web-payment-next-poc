import React from 'react';
import { cdnSubPath } from '@/shared/utils/string/path';

import Style from './style';

const TopRibbon = () => {
  return (
    <Style>
      <div className="top-ribbon">
        <img src={`${cdnSubPath()}/images/virgo-full.png`} width={80} height={41} alt="virgo" />
        <div className="top-ribbon_check-wrapper">
          <img src={`${cdnSubPath()}/images/ribbon-check.png`} alt="ribbon check" />
        </div>
      </div>
    </Style>
  );
};

export default TopRibbon;
