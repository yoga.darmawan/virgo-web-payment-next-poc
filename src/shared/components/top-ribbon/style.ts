import styled from '@emotion/styled';
import { css } from '@emotion/react';

import { mediaQuery } from '@/assets/styles/util/media-query';
import { parseSpace } from '@/assets/styles/util/space';

const Style = styled.div`
  ${({
    theme: {
      palette,
      customComponents: { topRibbon },
    },
  }) => css`
    .top-ribbon {
      display: flex;
      justify-content: space-between;
      align-items: center;
      background-color: ${palette.primary.lighter};
      ${mediaQuery({
        padding: parseSpace(topRibbon.padding),
        height: topRibbon.height,
      })}
    }
    .top-ribbon_check-wrapper {
      position: relative;
      height: 100%;
      ${mediaQuery({
        width: topRibbon.checkWidth,
      })}
      img {
        height: 100%;
      }
    }
  `}
`;

export default Style;
