import styled from '@emotion/styled';
import { css } from '@emotion/react';

import { mediaQuery } from '@/assets/styles/util/media-query';

export const ModalWrapperStyle = styled.div`
  ${({
    theme: {
      palette: { common },
      customComponents: { modal },
    },
  }) => css`
    ${mediaQuery({
      backgroundColor: [null, common.white],
      outline: ['none'],
      position: ['relative', 'absolute'],
      top: [null, '50%'],
      left: [null, '50%'],
      transform: [null, 'translate(-50%, -50%)'],
      width: ['100%'],
      borderRadius: [null, '16px'],
      boxSizing: [null, 'border-box'],
      maxWidth: modal.maxWidth,
    })}
  `}
`;

export const BottomSheetWrapperStyle = styled.div`
  ${({
    theme: {
      palette: { common, mist },
    },
  }) => css`
    overflow-y: scroll;
    &::-webkit-scrollbar {
      width: 0; /* Remove scrollbar space */
      background: transparent; /* Optional: just make scrollbar invisible */
    }
    .handlebar {
      background-color: ${common.white};
      position: absolute;
      left: 0;
      top: 0;
      right: 0;
      -ms-touch-action: none;
      touch-action: none;
      user-select: none;
      display: flex;
      justify-content: center;
      padding-top: 1rem;
      cursor: pointer;
      padding-bottom: 8px;
      div {
        width: 42px;
        height: 6px;
        border-radius: 100px;
        background-color: ${mist.main};
      }
    }
    .content {
      padding-top: 32px;
    }
  `}
`;
