import React from 'react';
import Modal from '@mui/material/Modal';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import { useTheme } from '@emotion/react';

import { BottomSheetWrapperStyle, ModalWrapperStyle } from './style';

interface Props {
  open: boolean;
  onOpen: (isOpen: boolean) => any;
  children: React.ReactElement;
}

const ModalBottomSheet = ({ open, onOpen, children }: Props) => {
  const theme = useTheme();
  const {
    breakpoints: { values: bp },
  } = theme;
  const width = window.innerWidth;
  return (
    <>
      <Modal open={open && width > bp.sm} onClose={() => onOpen(false)}>
        <ModalWrapperStyle>{children}</ModalWrapperStyle>
      </Modal>
      <SwipeableDrawer
        anchor="bottom"
        disableSwipeToOpen
        open={open && width <= bp.sm}
        onClose={() => onOpen(false)}
        onOpen={() => onOpen(true)}
        PaperProps={{
          sx: {
            borderTopLeftRadius: 14,
            borderTopRightRadius: 14,
            overflow: 'hidden',
          },
        }}
      >
        <BottomSheetWrapperStyle>
          <div className="handlebar">
            <div></div>
          </div>
          <div className="content">{children}</div>
        </BottomSheetWrapperStyle>
      </SwipeableDrawer>
    </>
  );
};

export default ModalBottomSheet;
