import { TopupChannelsResponse } from './entity';

export default interface MimoUseCase {
  viewTopupPage: () => Promise<TopupChannelsResponse>;
}
