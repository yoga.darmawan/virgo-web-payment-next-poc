export interface TopupChannel {
  channel_id?: string;
  channel_name?: string;
  channel_logo_url?: string;
  group_id?: string;
  group_name?: string;
  group_logo_url?: string;
  group_type?: string;
  virtual_code?: string;
  instructions?: string[];
  copiable_data?: {
    label?: string;
    value?: string;
  };
  footer_notes?: {
    label?: string;
    value?: string;
  };
}

export interface TopupChannelsResponse {
  data?: {
    topup_channels: TopupChannel[];
  };
}
