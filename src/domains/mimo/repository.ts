import { TopupChannelsResponse } from './entity';

export default interface MimoRepository {
  getTopupChannels: () => Promise<TopupChannelsResponse>;
}
