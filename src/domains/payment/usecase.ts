import { PaymentNonce, PaymentNonceResponse } from './entity';

export default interface PaymentUseCase {
  verifyPartnerPaymentPasscode: (payload: PaymentNonce) => Promise<PaymentNonceResponse>;
}
