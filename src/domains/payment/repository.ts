import { PaymentNonce, PaymentNonceResponse } from './entity';

export default interface PaymentRepository {
  verifyPartnerPaymentPasscode: (payload: PaymentNonce) => Promise<PaymentNonceResponse>;
  requestPaymentNonce: (payload: PaymentNonce, token: string) => Promise<PaymentNonceResponse>;
}
