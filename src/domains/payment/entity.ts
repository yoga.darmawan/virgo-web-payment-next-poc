export interface PartnerPaymentQuery {
  payment_info?: string;
  callback_url?: string;
}

export interface PartnerPaymentSuccessQuery {
  payment_nonce?: string;
  callback_url?: string;
}

export interface PaymentNonce {
  payment_info: string;
  callback: string;
  passcode: string;
}

export interface PaymentNonceResponse {
  data?: {
    payment_nonce: string;
  };
}
