import { OneTimePaymentLogin, LoginResponse } from './entity';

export default interface PaymentUseCase {
  oneTimePaymentLogin: (payload: OneTimePaymentLogin) => Promise<LoginResponse>;
}
