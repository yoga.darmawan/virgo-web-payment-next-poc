export interface OneTimePaymentLogin {
  token: string;
  payment_auth: string;
}

export interface Session {
  isPartnerPayment?: boolean;
  isOneTimePayment?: boolean;
  token: string;
}

export interface LoginResponse {
  redirect_url: string;
}
