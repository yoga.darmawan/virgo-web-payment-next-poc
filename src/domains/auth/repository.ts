import { OneTimePaymentLogin, LoginResponse } from './entity';

export default interface AuthRepository {
  oneTimePaymentLogin: (payload: OneTimePaymentLogin) => Promise<LoginResponse>;
}
