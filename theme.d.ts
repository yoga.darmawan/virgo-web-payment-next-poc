import { Theme, ThemeOptions } from '@/assets/styles/theme/type';
import { TypographyOptions, Typography } from '@/assets/styles/theme/typography/type';

declare module '@mui/material/styles' {
  interface TypographyVariants extends TypographyOptions {}

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions extends TypographyOptions {}
}

// Update the Typography's variant prop options
declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides extends Typography {}
}
declare module '@mui/material/styles' {
  export function createTheme(options?: ThemeOptions): Theme;
}
