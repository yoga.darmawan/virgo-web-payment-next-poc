import React from 'react';
// import dynamic from 'next/dynamic';
// import View from '@/modules/mimo/presentation/interface/top-up';
import withTopRibbon from '@/shared/components/layouts/with-top-ribbon';

// const ViewNoSSR = dynamic(() => import('@/modules/mimo/presentation/interface/top-up'), {
//   ssr: false,
// });

// PAP = Payment At Partner
const dummyEntryPointUrlForPAP =
  '/payment?payment_info=UNyiH%20yshc9Noys/9FwmeUx3ir/O3iNR7JzQLNMKr2czn0zHH1P3T7EacB%208PvBhnNcjPmATGU7g60kHr5B87NAXWQzqpUy5GoG0B5a7UNot01YYJDeEkMBRn1Cedexvj9UtomrbfJnc3mPlQRm8G%20R1UwuugoSrvtEzCGSMbzpGsaal%20iigLNZRXWzpaeMvXM/m5NKaKk2z74QYBARab8yBrGN6F1OryCRsYrhFa5HcB/5/791wPj7uoFhph1DzH2a3UrJHG3UZOGY5Az%20683baqrP62O9WL%20%20r%20UXhcTQeNJPNGTbW56wLYkl9XjbDxNuoKcGyG8WGFtWG%20g4/4uGpFqMSg3UFvZDPB/meCSN2dieX8QghZzPbwZYZqolv/pfSAivuqnXAgmUUQ6IEMXA%2081nQl4Y8sBVa6rBkUpFdqy87nW2MAvzFSRMUlw/qX%20JHEcp4M1LnZ2LU/f%206VgzWHsBv/xJ3t14l6mv/K/kH8Xk/aZY1ITPGKuUSdeh1ZFxmYJaB5Sa17UOacskNyQIQNXSTN%20fT&callback_url=https://mock-dev.virgoku.dev?partner_reference_code=LOLLAOL';

const dummyToken =
  'eyJhbGciOiJSUzI1NiIsImtpZCI6InB1YmxpYzpiYzNmMWVjNC1mZjgyLTRhNzMtYTFmNy1lNWUzMTYxMWMyY2MiLCJ0eXAiOiJKV1QifQ.eyJhdWQiOltdLCJjbGllbnRfaWQiOiIxNDMzMjU1MTAwOTA5OTgxNjk2IiwiZXhwIjoxNjUwMjcwNjMzLCJleHQiOnt9LCJpYXQiOjE2NTAyNjcwMzMsImlzcyI6Imh0dHBzOi8vYXV0aC1kZXYudmlyZ29rdS5kZXYvIiwianRpIjoiYjgwYTQyOGMtYzY3Yi00YTllLTlhNTYtNWIwY2U3Y2Q2Zjc1IiwibmJmIjoxNjUwMjY3MDMzLCJzY3AiOlsib25ldGltZXBheW1lbnQubGluayJdLCJzdWIiOiIxNDMzMjU1MTAwOTA5OTgxNjk2In0.fG-lVB-pf49-APppMRcHo0lGQozWhDoIf9TDyWzXzmCz0Qu_a7FAAwAVLSdsVGc64iJW6SdyMmEI6_xJXGe5sSh4827b8aYmvDE3ACvUO5yCF3deLJ7NQ3aFLzPilJj41QjbGqSvEJqH9GXDQEIGn4oTkRUgsMGTFFydc6hTauD-UaMckv7kNKgmQlVnWGu6QQrTVpNz6lw4INIHASsPdSp0dNfCUgTPPF0VMe8VFx4MAAj8iIi5LLmbi9EUEolGTdqNP2fZAywm2MpxuzYfOwlhHeA_wR9kfw23yB1GpvehMDYMElktj6pk3FeKmBrHczLfva88_2wdBB5nVJQ0lTbYcd_WDRRRXd6A99Uirs1hr1tsFkpokHDYnT3iS7Jhl9dKRN7RTzcDMYh7nhqHLbmLObcEiveVCjsrP7z0ydg_YwXi7di0NOzqeYv-pYvMUvK8TpPoAQPipVDEBAgy03wFHny1BpCQgwa84y7Qq7ppluj-m8VqStaHqqQsIJHO5LGDZmyS0FnUSulPynfK_wrZU5hS7VC3ky4-33X8WQ9GYtUTi9xKq1IdWJFUuU2LSQEnBAW8uqf5fNCLMl8KveQd6uFPHf0ROkjJLWfSF_UAeuxjQDGITFynToGQOwcNuI-vZsYZJU4uq8qfw0FjZWxMPWBL_5esWzeU87bw8L0';

const dummyEntryPointUrlFor1xp = `/one-time-payment/consent-callback#access_token=${dummyToken}&state=799uuhuji9jaskdi0eejduin`;

const on1xpRedirect = () => {
  window.location.href = dummyEntryPointUrlFor1xp;
};

const Component = () => {
  return (
    <>
      <div>halaman partner</div>
      <form action={dummyEntryPointUrlForPAP} method="POST">
        <input type="text" name="token" defaultValue={dummyToken} hidden />
        <p>payment at partner</p>
        <button type="submit">create payment at partner transaction</button>
      </form>
      <p>one time payment</p>
      <button type="button" onClick={on1xpRedirect}>
        redirect to one time payment entry point after login
      </button>
    </>
  );
};

const PartnerPage = (props: any) => <Component {...props} />;

PartnerPage.getLayout = withTopRibbon;

export default PartnerPage;
