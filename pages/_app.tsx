import 'normalize.css/normalize.css';

import type { AppProps } from 'next/app';
import type { ReactElement, ReactNode } from 'react';
import type { NextPage } from 'next';
import { Global, css, ThemeProvider } from '@emotion/react';

import { wrapper } from '@/shared/utils/redux/store';
import OverlayLoading from '@/shared/components/loading/overlay';

import theme from '@/assets/styles/theme/index';
import GlobalStyle from '@/assets/styles/global';
import SpaceStyle from '@/assets/styles/space';
import FontStyle from '@/assets/styles/font';
import AlignmentStyle from '@/assets/styles/alignment';
import DimensionStyle from '@/assets/styles/dimension';
import InteractionStyle from '@/assets/styles/interaction';
import { Theme } from '@/assets/styles/theme/type';

/* eslint-disable */
type NextPageWithLayout = NextPage & {
  getLayout?: (page?: ReactElement) => ReactNode;
};
/* eslint-enable */

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page);
  return (
    <>
      <Global
        styles={css`
          ${SpaceStyle}
          ${FontStyle}
              ${AlignmentStyle}
              ${DimensionStyle}
              ${InteractionStyle}
        `}
      />
      <ThemeProvider theme={theme}>
        {getLayout(
          <>
            <Global
              styles={(props) => css`
                ${GlobalStyle(props as Theme)}
              `}
            />
            <OverlayLoading />
            <Component {...pageProps} />
          </>,
        )}
      </ThemeProvider>
    </>
  );
}

export default wrapper.withRedux(MyApp);
