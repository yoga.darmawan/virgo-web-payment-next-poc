import React from 'react';
import { GetServerSidePropsContext } from 'next';
import dynamic from 'next/dynamic';

import withTopRibbon from '@/shared/components/layouts/with-top-ribbon';
import { withSessionSsr } from '@/shared/utils/session';

const ViewNoSSR = dynamic(
  () => import('@/modules/payment/presentation/interface/partner-payment/payment-success'),
  {
    ssr: false,
  },
);

const PaymentSuccessPage = (props: any) => <ViewNoSSR {...props} />;

PaymentSuccessPage.getLayout = withTopRibbon;

export const getServerSideProps = withSessionSsr(async function handler({
  req,
}: GetServerSidePropsContext) {
  const { session } = req;
  const { token, isPartnerPayment } = session;
  if (isPartnerPayment && token) {
    return {
      props: {},
    };
  }
  return {
    notFound: true,
  };
},
'PartnerPayment');

export default PaymentSuccessPage;
