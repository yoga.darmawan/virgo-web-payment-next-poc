import React from 'react';
import { GetServerSidePropsContext } from 'next';
import dynamic from 'next/dynamic';

import withTopRibbon from '@/shared/components/layouts/with-top-ribbon';
import { withSessionSsr } from '@/shared/utils/session';

const ViewNoSSR = dynamic(
  () => import('@/modules/payment/presentation/interface/one-time-payment/confirmation'),
  {
    ssr: false,
  },
);

const ConfirmationPage = (props: any) => <ViewNoSSR {...props} />;

ConfirmationPage.getLayout = withTopRibbon;

export const getServerSideProps = withSessionSsr(function handler({
  req,
}: GetServerSidePropsContext) {
  const { session } = req;
  if (session.isOneTimePayment) {
    return {
      props: {},
    };
  }
  return {
    notFound: true,
  };
},
'OneTimePayment');

export default ConfirmationPage;
