import React from 'react';
import dynamic from 'next/dynamic';

import withTopRibbon from '@/shared/components/layouts/with-top-ribbon';

const ViewNoSSR = dynamic(
  () => import('@/modules/payment/presentation/interface/one-time-payment/consent-callback'),
  {
    ssr: false,
  },
);

const PaymentPage = (props: any) => <ViewNoSSR {...props} />;

PaymentPage.getLayout = withTopRibbon;

export const getServerSideProps = function handler() {
  return {
    props: {},
  };
};

export default PaymentPage;
