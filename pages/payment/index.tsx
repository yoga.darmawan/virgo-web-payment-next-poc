import React from 'react';
import { GetServerSidePropsContext } from 'next';
import bodyParser from 'body-parser';
import util from 'util';
import dynamic from 'next/dynamic';

import withTopRibbon from '@/shared/components/layouts/with-top-ribbon';
import { withSessionSsr } from '@/shared/utils/session';

const ViewNoSSR = dynamic(
  () => import('@/modules/payment/presentation/interface/partner-payment/verify-passcode'),
  {
    ssr: false,
  },
);

const getBody = util.promisify(bodyParser.urlencoded());

const PaymentPage = (props: any) => <ViewNoSSR {...props} />;

PaymentPage.getLayout = withTopRibbon;

export const getServerSideProps = withSessionSsr(async function handler({
  req,
  res,
}: GetServerSidePropsContext) {
  if (req.method === 'POST') {
    await getBody(req, res);
    const { token } = req?.body;
    if (token) {
      req.session.isPartnerPayment = true;
      req.session.token = token;
      await req.session.save();
      return {
        props: {},
      };
    } else {
      return {
        notFound: true,
      };
    }
  } else if (req.method === 'GET') {
    const {
      session: { token, isPartnerPayment },
    } = req;
    if (token && isPartnerPayment) {
      return {
        props: {},
      };
    } else {
      return {
        notFound: true,
      };
    }
  }
  return {
    notFound: true,
  };
},
'PartnerPayment');

export default PaymentPage;
