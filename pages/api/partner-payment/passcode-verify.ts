import { NextApiRequest, NextApiResponse } from 'next';

import { withSessionApi } from '@/shared/utils/session';
import { repository } from '@/modules/payment/util';

export default withSessionApi(async function handler(req: NextApiRequest, res: NextApiResponse) {
  console.log('passcode verify request from client');
  console.log(req.body);
  console.log(req.session);
  const { session } = req;
  await repository.requestPaymentNonce(req.body, session.token);
  const response = {
    payment_nonce: 'euhdh9h9und9jwi9dm29iejdundcjj0isj9j29jdi2dokasniubrf9uhej',
  };
  res.status(200).json(response);
  res.send('passcode verify');
}, 'PartnerPayment');
