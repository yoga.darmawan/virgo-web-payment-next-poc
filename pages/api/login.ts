import { NextApiRequest, NextApiResponse } from 'next';
import { withSessionApi } from '@/shared/utils/session';

export default withSessionApi(async function handler(req: NextApiRequest, res: NextApiResponse) {
  console.log(req.body);
  req.session.token = 'lololololol';
  await req.session.save();
  res.send('Logged in');
}, 'PartnerPayment');
