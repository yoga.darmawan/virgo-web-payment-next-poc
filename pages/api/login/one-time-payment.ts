import { NextApiRequest, NextApiResponse } from 'next';

import { withSessionApi } from '@/shared/utils/session';
import { generateUrl } from '@/shared/utils/router/redirect';

export default withSessionApi(async function useHandler(req: NextApiRequest, res: NextApiResponse) {
  console.log('one time payment login from client');
  console.log(req.body);
  const { session } = req;
  session.isOneTimePayment = true;
  session.token = req.body.token || '';
  await session.save();
  const response = {
    redirect_url: generateUrl({ name: 'OneTimePaymentConfirmation', id: req.body.payment_auth }),
  };
  res.status(200).json(response);
}, 'OneTimePayment');
